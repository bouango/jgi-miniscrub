import random, sys


def find_candidates(genome_name, samname):
	candidates, genome_len = [], 0
	with(open(samname, 'r')) as samfile:
		for line in samfile:
			if line.startswith('@'):
				if 'SN' in line and genome_name in line:
					genome_len = int(line.strip().split(':')[-1])
			else:
				splits = line.strip().split('\t')
				if splits[2] != genome_name:
					continue
				candidate_len = 0
				for i in splits:
					if i.startswith('ZQ:i:'):
						candidate_len = int(i.split(':')[-1])
				if candidate_len != 0:
					candidates.append([splits[0], candidate_len])
	return candidates, genome_len


def choose_candidates(covg, candidates, genome_len):
	#selections, target, current = [], covg * genome_len, 0
	selections, target, current = {}, covg * genome_len, 0
	while current < target and len(candidates) > 0:
		rand_ind = int(random.random() * len(candidates))
		#selections.append(candidates[rand_ind][0])
		selections[candidates[rand_ind][0]] = 0
		current += candidates[rand_ind][1]
		candidates.remove(candidates[rand_ind])
	return selections


def write_output(inname, outname, selections):
	counter, write = 0, False
	with(open(inname, 'r')) as infile:
		with(open(outname, 'w')) as outfile:
			for line in infile:
				if counter == 0:
					name = line[1:].split(' ')[0]
					if name in selections:
						write = True
					else:
						write = False
				if write:
					outfile.write(line)
				counter = (counter + 1) % 4


def main():
	if len(sys.argv) != 6:
		print('Usage: python downsample.py <coverage> <genome_name> <input sam> <input fastq> <output fastq>')
		sys.exit()
	covg, genome_name, samname, inname, outname = int(sys.argv[1]), sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]

	candidates, genome_len = find_candidates(genome_name, samname)
	selections = choose_candidates(covg, candidates, genome_len)
	write_output(inname, outname, selections)


if __name__ == '__main__':
	main()

